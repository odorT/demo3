import requests
import os
import time

url = f'http://{os.environ["EC2_PUBLIC_IP"]}:8080/actuator/health'

for i in range(5):
    response = str(requests.get(url))
    if "200" in response:
        print("Application is up and running")
        break
    else:
        print(f"Cannot reach the given url: {url}")
        time.sleep(10)
else:
    print("Application is down")
    exit(1)