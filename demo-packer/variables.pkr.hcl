variable "ssh_username" {
  type        = string
  description = "The ssh username to authenticate"
  default     = "ec2-user"
  sensitive   = true
}
variable "ssh_key_pub" {
  type = string
  description = "The ssh public key"
  default = ""
  sensitive = true
}
variable "ami_name" {
  type        = string
  description = "The AMI name that will be created"
  default     = "demo3-kamran-ami"
}
variable "instance_type" {
  type        = string
  description = "The instance type"
  default     = "t2.micro"
}
variable "region" {
  type        = string
  description = "The region that instance will be created"
  default     = "us-east-2"
}
variable "vpc_id" {
  type        = string
  description = "VPC id"
  default     = "	vpc-6feb7704"
}
variable "subnet_id" {
  type        = string
  description = "Subnet id"
  default     = "subnet-58e6bc14"
}
variable "sg_id" {
  type        = string
  description = "The Security group id"
  default     = "sg-034b108058ea74d83"
}
