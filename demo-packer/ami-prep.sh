#!/bin/bash

sudo yum update -y
sudo yum install -y https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
sudo yum install -y mysql-community-client git
sudo amazon-linux-extras install java-openjdk11
echo ${ssh_key_pub} > /home/ec2-user/.ssh/authorized_keys
