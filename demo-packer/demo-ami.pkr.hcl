# AWS EBS backded AMI building

source "amazon-ebs" "demo-ec2-build" {
  ssh_username      = var.ssh_username
  ami_name          = var.ami_name
  instance_type     = var.instance_type
  region            = var.region
  vpc_id            = var.vpc_id
  subnet_id         = var.subnet_id
  security_group_id = var.sg_id
  
  communicator = "ssh"
  ssh_clear_authorized_keys   = true
  ssh_timeout                 = "5m"
  associate_public_ip_address = true
  force_deregister            = true
  force_delete_snapshot       = true
  source_ami                  = "ami-0d8d212151031f51c" // Default Amazon Linux 2 image
  launch_block_device_mappings {
    device_name           = "/dev/xvda"
    volume_size           = 8
    volume_type           = "gp2"
    delete_on_termination = true
  }
}

build {
  sources = ["source.amazon-ebs.demo-ec2-build"]

  provisioner "shell" {
    script = "ami-prep.sh"
  }
}
