# EC2 configuration variables

variable "instance_type" {
  default = "t2.large"
}

variable "aws_region" {
  default = "us-east-2"
}

variable "ec2-ami" {
  description = "Custom made AMI (includes java, mysql and all base packages to run application)"
  default = "ami-0a6709ebe0f8d9d1d"
}

variable "associate_public_ip_address" {
  default = true
}

variable "ssh_key" {
  default = ""
}

variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

# RDS configuration variables

variable "db_instance_class" {
  default = "db.t2.large"
}

variable "db_identifier" {
  default = "demo3-rds-kamran"
}

variable "mysql_name" {
  default = ""
}

variable "mysql_user" {
  default = ""
}

variable "mysql_pass" {
  default = ""
}
variable "mysql_port" {
  default = ""
}