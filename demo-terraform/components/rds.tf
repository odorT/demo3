resource "aws_db_parameter_group" "demo3-db-pg-kamran" {
  name   = "demo3-rds-pg-kamran"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_subnet_group" "demo3-kamran-db-subnet-group" {
  name       = "demo3-kamran-db-subnet-group-kamran"
  subnet_ids = [module.vpc.subnet_private1_id, module.vpc.subnet_private2_id]

  tags = {
    Name = "DB subnet Group for RDS"
  }
}

resource "aws_security_group" "demo3-rds-sg-kamran" {
  name        = "rds-security-group-kamran"
  description = "Security group for RDS dbs"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = [module.vpc.vpc_cidr]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = [module.vpc.vpc_cidr]
  }
}

resource "aws_db_instance" "demo3-rds-kamran" {
  allocated_storage      = 100
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = var.db_instance_class
  identifier             = var.db_identifier
  name                   = var.mysql_name
  username               = var.mysql_user
  password               = var.mysql_pass
  parameter_group_name   = aws_db_parameter_group.demo3-db-pg-kamran.name
  db_subnet_group_name   = aws_db_subnet_group.demo3-kamran-db-subnet-group.name
  vpc_security_group_ids = [aws_security_group.demo3-rds-sg-kamran.id]
  publicly_accessible    = false
  skip_final_snapshot    = true
  multi_az               = false
}