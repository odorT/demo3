provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.aws_region
}

module "vpc" {
  source      = "../modules/"
  public_az   = data.aws_availability_zones.available.names[0]
  private1_az = data.aws_availability_zones.available.names[0]
  private2_az = data.aws_availability_zones.available.names[1]
}

data "aws_availability_zones" "available" {
  state = "available"
}