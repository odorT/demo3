data "aws_ami" "demo3-ami" {
  most_recent      = true
  owners           = ["973111631606"]

  filter {
    name   = "name"
    values = ["demo3-kamran-ami"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "public-ec2" {
  depends_on = [module.vpc.vpc_id, module.vpc.igw_id, aws_db_instance.demo3-rds-kamran]

  ami                         = data.aws_ami.demo3-ami.id
  instance_type               = var.instance_type
  subnet_id                   = module.vpc.subnet_public_id
  vpc_security_group_ids      = [aws_security_group.demo3-ec2-sg.id]
  associate_public_ip_address = var.associate_public_ip_address

  tags = {
    Name = "(Kamran) EC2 instance for running application"
  }

  user_data = <<EOF
#!/bin/bash

cd /home/ec2-user

# Exporting environment variables

export MYSQL_HOST=${aws_db_instance.demo3-rds-kamran.address}
export MYSQL_PORT=${var.mysql_port}
export MYSQL_NAME=${var.mysql_name}
export MYSQL_PASS=${var.mysql_pass}
export MYSQL_USER=${var.mysql_user}
export MYSQL_URL=jdbc:mysql://${aws_db_instance.demo3-rds-kamran.address}:${var.mysql_port}/${var.mysql_name}
export AWS_ACCESS_KEY_ID=${var.aws_access_key}
export AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}

# Getting jar from aws s3
aws s3 cp s3://demo3-kamran/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar .
sudo mv *.jar app.jar

# Running application jar
/usr/bin/java -Xms128m -Xmx256m -Dspring.profiles.active=mysql -jar app.jar &

EOF
}

resource "aws_security_group" "demo3-ec2-sg" {
  name        = "demo3-ec2-sg"
  description = "Security group for ec2 instances"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    description = "HTTPS"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
