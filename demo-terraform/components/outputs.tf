output "ec2-public-ip" {
  description = "The public ip assigned to ec2 instance which runs application"
  value = aws_instance.public-ec2.public_ip
}
output "ec2-public-dns" {
  description = "The public dns url of ec2"
  value = aws_instance.public-ec2.public_dns
}