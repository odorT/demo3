resource "aws_vpc" "demo3-kamran-vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = var.tenancy
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_internet_gateway" "demo3-kamran-igw" {
  vpc_id = aws_vpc.demo3-kamran-vpc.id

  tags = {
    Name = "${var.vpc_name}-IGW"
  }
}

resource "aws_route" "demo3-kamran-public-rt" {
  route_table_id         = aws_vpc.demo3-kamran-vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.demo3-kamran-igw.id
}

resource "aws_subnet" "demo3-kamran-public-sub" {
  vpc_id            = aws_vpc.demo3-kamran-vpc.id
  cidr_block        = var.public_cidr
  availability_zone = var.public_az

  tags = {
    Name = "${var.vpc_name}-public-subnet"
  }
}

resource "aws_subnet" "demo3-kamran-priv-sub1" {
  vpc_id            = aws_vpc.demo3-kamran-vpc.id
  cidr_block        = var.private1_cidr
  availability_zone = var.private1_az

  tags = {
    Name = "${var.vpc_name}-private-sub1"
  }
}

resource "aws_subnet" "demo3-kamran-priv-sub2" {
  vpc_id            = aws_vpc.demo3-kamran-vpc.id
  cidr_block        = var.private2_cidr
  availability_zone = var.private2_az

  tags = {
    Name = "${var.vpc_name}-private-sub2"
  }
}

resource "aws_eip" "demo3-kamran-eip" {
  depends_on = [aws_internet_gateway.demo3-kamran-igw]
  vpc        = true

  tags = {
    Name = "${var.vpc_name}-EIP"
  }
}

resource "aws_nat_gateway" "demo3-kamran-natgateway" {
  subnet_id     = aws_subnet.demo3-kamran-public-sub.id
  allocation_id = aws_eip.demo3-kamran-eip.id

  tags = {
    Name = "${var.vpc_name}-NATgateway"
  }
}

resource "aws_route_table" "demo3-kamran-private-rt" {
  vpc_id = aws_vpc.demo3-kamran-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.demo3-kamran-natgateway.id
  }

  tags = {
    Name = "${var.vpc_name}-private-rt"
  }
}

resource "aws_route_table_association" "demo3-kamran-public-rt-asso" {
  subnet_id      = aws_subnet.demo3-kamran-public-sub.id
  route_table_id = aws_vpc.demo3-kamran-vpc.main_route_table_id
}

resource "aws_route_table_association" "demo3-kamran-private1-rt-asso" {
  subnet_id      = aws_subnet.demo3-kamran-priv-sub1.id
  route_table_id = aws_route_table.demo3-kamran-private-rt.id
}

resource "aws_route_table_association" "demo3-kamran-private2-rt-asso" {
  subnet_id      = aws_subnet.demo3-kamran-priv-sub2.id
  route_table_id = aws_route_table.demo3-kamran-private-rt.id
}
