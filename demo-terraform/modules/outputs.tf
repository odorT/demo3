output "vpc_id" {
  value = aws_vpc.demo3-kamran-vpc.id
}

output "vpc_arn" {
  value = aws_vpc.demo3-kamran-vpc.arn
}

output "vpc_cidr" {
  value = aws_vpc.demo3-kamran-vpc.cidr_block
}

output "igw_id" {
  value = aws_internet_gateway.demo3-kamran-igw.id
}

output "subnet_public_id" {
  value = aws_subnet.demo3-kamran-public-sub.id
}

output "subnet_private1_id" {
  value = aws_subnet.demo3-kamran-priv-sub1.id
}

output "subnet_private2_id" {
  value = aws_subnet.demo3-kamran-priv-sub2.id
}

output "private_rt_id" {
  value = aws_route_table.demo3-kamran-private-rt.id
}